
package com.mycompany.menudecolas;

/**
 *
 * @author Jherson Rodrigo Mamani Poma // Mil Disculpas Inge. este ejercicio no lo acabe pero el otro ejercicio si lo hice
 */
public class nodo<T> {
    private T element;//informacion
    private nodo<T> next;//referencia
    
    public nodo(T element, nodo<T> next){//constructor del nodo
        this.element = element;
        this.next = next;
    }

    public T getElement() {
        return element;
    }

    public void setElement(T element) {
        this.element = element;
    }

    public nodo<T> getNext() {
        return next;
    }

    public void setNext(nodo<T> next) {
        this.next = next;
    }  

}
