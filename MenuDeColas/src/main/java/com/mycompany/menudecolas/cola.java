
package com.mycompany.menudecolas;

/**
 *
 * @author Jherson Rodrigo Mamani Poma // Mil Disculpas Inge. este ejercicio no lo acabe pero el otro ejercicio si lo hice
 */
public class cola<T> {
    private nodo<T> first;
    private nodo<T> last;
    private int size;
    
    public cola(){
        this.first = null;
        this.last = null;
        this.size = 0;
    }
    
    public boolean isEmpty(){
        return first == null;
    }
    
    public int size(){
        return size;
    }
    
    public T first(){
        if(isEmpty()){
            return null;
        }
        return first.getElement();
    }
    
    public void enqueue(T element){
        nodo<T> newelement = new nodo(element, null);
        if(isEmpty()){
            first = newelement;
            last = newelement;
        }else{
            if(size() == 1){
                first.setNext(newelement);
            }else{
                last.setNext(newelement);
            }
            last = newelement;
        }
        size++;
    }
    
    
    public T dequeue(){
        if(isEmpty()){
            return null;
        }
        T element = first.getElement();
        nodo<T> aux = first.getNext();
        first = aux;
        size--;
        if(isEmpty()){
            last = null;
        }
        return element;
    }

    
}
