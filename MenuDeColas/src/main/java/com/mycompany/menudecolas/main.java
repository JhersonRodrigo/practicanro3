
package com.mycompany.menudecolas;

import java.util.Scanner;

/**
 *
 * @author Jherson Rodrigo Mamani Poma // Mil Disculpas Inge. este ejercicio no lo acabe pero el otro ejercicio si lo hice
 */
public class main {
    public static void main (String[] args){
        
        Scanner sc = new Scanner (System.in);
        System.out.println("=============================");
        System.out.println("Menu");
        System.out.println("=============================");
        System.out.println("1. Introducir datos");
        System.out.println("2. Borrar datos");
        System.out.println("3. Sacar todos los elementos");
        System.out.println("4. Mostrar el elemento minimo y maximo");
        
        cola <Integer> cd = new cola<>(); 
        
        int selected;
        int continues=0;
        
        while(continues == 0){
            System.out.println("Que opcion va elegir");
            selected = sc.nextInt();
            switch(selected){
                case 1:
                   int n = sc.nextInt();
            }
        }
        System.out.println("El valor maximo de integer es :"+Integer.MAX_VALUE);

        sortQueue(cd);
        while(!cd.isEmpty()){
            System.out.print(cd.dequeue()+" ");
        }
   
    }
    
    
    public static void sortQueue(cola c){
        for(int i = 1; i<= c.size();i++){
            int index_min = indexMin(c,c.size()-i);
            insertMin(c,index_min);
        }
    }
    
    public static int indexMin(cola<Integer> c,int indexSort){
            int index_min = -1;
            int value = Integer.MAX_VALUE;
            int size = c.size();
            
            for(int i = 0;i<size;i++){
                int current = c.first();
                c.dequeue();//
                if(current <= value && i <= indexSort){
                    index_min = i;
                    value = current;
                }
                c.enqueue(current);
            }
            
            return index_min;
    }
    
    public static void insertMin(cola<Integer> c, int index_min){
        int value = 0;
        int size = c.size();
        
        for(int i = 0; i < size;i++){
               int current = c.first();
               c.dequeue();
               if(i!=index_min){
                   c.enqueue(current);
               }else{
                   value = current;
               }
        }
        c.enqueue(value);
    }

}
