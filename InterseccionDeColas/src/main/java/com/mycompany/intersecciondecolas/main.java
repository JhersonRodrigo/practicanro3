
package com.mycompany.intersecciondecolas;
import java.util.Collections;
import java.util.LinkedList;

/**
 *
 * @author Jherson Rodrigo Mamani Poma
 */
public class main {
    
    static LinkedList cola1 = new LinkedList();
    static LinkedList cola2 = new LinkedList();
    static LinkedList colaInterseccion = new LinkedList();
    static LinkedList cola1Aux = new LinkedList();
    static LinkedList cola2Aux = new LinkedList();
    
    public static boolean buscar(int dato){
        boolean c = false;
        int d;
        while( cola1Aux.size() > 0){
            d=(int) cola1Aux.removeLast();
            if(dato == d)
                c = true;
            cola2Aux.addFirst(d);
        }
        while( cola2Aux.size()>0)
            cola1Aux.addFirst(cola2Aux.removeLast());
        return c;
    }
    
    public static void main(String[] args){
        int dato;
        cola1.addFirst(1);
        cola1.addFirst(3);
        cola1.addFirst(4);
        cola1.addFirst(6);
        cola1.addFirst(8);
        cola1.addFirst(11);
        cola2.addFirst(1);
        cola2.addFirst(4);
        cola2.addFirst(5);
        cola2.addFirst(8);
        cola2.addFirst(10);
        cola2.addFirst(11);
        while(cola2.size()>0){
            dato = (int) cola2.removeLast();
            cola1Aux.addFirst(dato);
        }
        while(cola1.size()>0){
            dato= (int) cola1.removeLast();
            if(buscar(dato))
                colaInterseccion.addFirst(dato);
        }
        while(colaInterseccion.size()>0)
            System.out.print("Dato: " + (int) colaInterseccion.removeLast() + "\n");
        System.out.print("\n\nInterseccion de colas");
    }
}
